## Puppeteer API

Simple service to use Puppeteer with the Stealth plugin

## Install

- You need [Node.js](https://nodejs.org/en/download/)

- You need [yarn](https://yarnpkg.com/getting-started/install)

- Run `yarn install`

## Running it locally after installation

`node server.js`

or using script

`./run.sh`

## How to use it

**GET /:url**

Parameters:

- **proxy**: without protocol, ie: 154.23.234.12:9876.

- **user_agent**: the user agent string to use. If empty the default user agent will be used.

- **timeout**: miliseconds, ie: 30000 (30 seconds).

- **fullpage**: the fullpage is loaded, by default the result is returned when the DOM content is loaded. Use this option when you notice that the data you need is not loaded.

- **headers**: set extra headers. As a JSON object.

- **evaluate**: it uses the `page.evaluate` function [[1](https://github.com/puppeteer/puppeteer/blob/main/docs/api.md#pageevaluatepagefunction-args)]. It can be used for extracting data from the DOM. For example, if there is an object you want to extract in the DOM, for example, in the location `windows.pdData`, you could extract that using this query parameter: `evaluate=windows.pdData`

- **click**: A selector to search for an element to click. If there are multiple elements satisfying the selector, the first one will be clicked. [[2](https://github.com/puppeteer/puppeteer/blob/main/docs/api.md#pageclickselector-options)]

- **click_wait**: miliseconds to wait for timeout after a **click** has been processed. Default to `5000`.

- **form**: JSON object. The inputs to be populated should be placed in the `fields` key as another JSON object (selector: value). If there is any selectable to be select, they should be placed in another JSON object into `select` (selector: value). A `submit` key should contain the selector of the submit button of the form, in other words, `submit` should be a selector pointing to the button to be "clicked".

For example:

```json
{
    "fields": {
        "#username": "batman",
        "#password": "iloverobin"
    },
    "select": {
        "#team": "superheroes"
    },
    "submit": "#submitbtn"
}
```

Also, we can specify an order for select combos. Useful for dynamic loading of options.

```json
{
    "fields": {
        "#username": "batman",
        "#password": "iloverobin"
    },
    "select": {
        "__order__": ["#team", "#color"],
        "#team": "superheroes",
        "#color": "red"
    },
    "submit": "#submitbtn"
}
```

- **screenshot**: take a page screenshot encoding as base64.

- **viewport**: for example `1366,768`.

- **waituntil**: similar to fullpage, fullpage use the default `load`. The default is `domcontentloaded`. You can use this property to specify what you want to do. For more information you can see the puppeteer documentation [here](https://github.com/puppeteer/puppeteer/blob/v5.5.0/docs/api.md#pagegotourl-options) Take a look at the `waitUntil` option.

- **wait**: other option to use `page.waitForTimeout` at the end. Not very useful but it's there just in case you need it.

- **jsoff**: disable the loading of javascript resources.

- **firefox**: to use Firefox instead of Chromium (default).

- **cookies**: list of cookies to set (JSON).

- **refilter**: filter request URLs matching this regular expression. It will include scripts resources anyway. You can use `jsoff` to exclude them. This option is useful when you want to do only some AJAX calls and exclude others. Useful when using in conjunction with `reajax` to make it faster.

- **reajax**: regular expression to match AJAX call URLs and return all of the resulting body contents in a list.


**Response Example**

```json
{
    "message": "All Done!",
    "errors": [],
    "data": {
        "url": "https://www.example.com/example.html",
        "body": "<html>...</html>",
        "cookies": [],
        "evaluate": {},
        "ajaxres": [],
        "screenshot": "data:image/png;base64,..."
    }
}
```


**IMPORTANT**

The `click` is done first, before the `form` submit.
