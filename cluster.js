const express = require('express');

const app = express();

const puppeteer = require('puppeteer-extra');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());


require('./config.js')(app, express);
require('./views/amzau.js')(app, puppeteer);
require('./views/main.js')(app, puppeteer);

app.listen(3000);
