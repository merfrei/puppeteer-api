const morgan = require('morgan');
const errorhandler = require('errorhandler');

module.exports = function(app, express){
    app.use(morgan('combined'));

    if (app.get('env') === 'development') {
        app.use(errorhandler({
            dumpExceptions: true,
            showStack: true
        }));
    }

    if (app.get('env') === 'production') {
        app.use(errorhandler());
    }
};
