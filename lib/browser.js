module.exports = {

    get_page_main_view: async function (req, puppeteer) {
        const errors = [];

        const chrome_args = ['--no-sandbox', '--disable-setuid-sandbox', '--incognito']
        if (req.query.viewport) {
            chrome_args.push('--window-size=' + req.query.viewport);
        }
        // PROXY
        if (req.query.proxy) {
            console.log('Proxy: ' + req.query.proxy + ' (' + req.params.url + ')');
            chrome_args.push('--proxy-server=' + req.query.proxy);
        }

        const firefox_args = ['-private'];

        let launch_params = { headless: true, args: chrome_args, ignoreHTTPSErrors: true, product: 'chrome'};
        if (req.query.firefox) {
            const extraPrefsFirefox = {};
            console.log('Using Firefox');
            if (req.query.proxy) {
                proxy = req.query.proxy.split(':');
                extraPrefsFirefox['network.proxy.type'] = 1;
                extraPrefsFirefox['network.proxy.http'] = proxy[0];
                extraPrefsFirefox['network.proxy.http_port'] = Number(proxy[1]);
                extraPrefsFirefox['network.proxy.ssl'] = proxy[0];
                extraPrefsFirefox['network.proxy.ssl_port'] = Number(proxy[1]);
                extraPrefsFirefox['network.proxy.ftp'] = proxy[0];
                extraPrefsFirefox['network.proxy.ftp_port'] = Number(proxy[1]);
                extraPrefsFirefox['network.proxy.socks'] = proxy[0];
                extraPrefsFirefox['network.proxy.socks_port'] = Number(proxy[1]);
                extraPrefsFirefox['network.proxy.share_proxy_settings'] = true;
                extraPrefsFirefox['network.proxy.backup.ssl'] = '';
                extraPrefsFirefox['network.proxy.backup.ssl_port'] = 0;
                extraPrefsFirefox['network.proxy.backup.socks'] = '';
                extraPrefsFirefox['network.proxy.backup.socks_port'] = 0;
                extraPrefsFirefox['network.proxy.backup.ftp'] = '';
                extraPrefsFirefox['network.proxy.backup.ftp_port'] = 0;
                extraPrefsFirefox['network.proxy.autoconfig_url'] = '';
                extraPrefsFirefox['network.proxy.autoconfig_url.include_path'] = false;
                extraPrefsFirefox['network.proxy.autoconfig_retry_interval_max'] = 300;
                extraPrefsFirefox['network.proxy.autoconfig_retry_interval_min'] = 5;
                extraPrefsFirefox['network.proxy.socks_version'] = 5;
                extraPrefsFirefox['network.proxy.no_proxies_on'] = 'localhost, 127.0.0.1';
                extraPrefsFirefox['network.proxy.failover_timeout'] = 1800;
                extraPrefsFirefox['network.proxy.proxy_over_tls'] = true;
                extraPrefsFirefox['network.proxy.socks_remote_dns'] = false;
                extraPrefsFirefox['media.peerconnection.identity.enabled'] = false;
                extraPrefsFirefox['media.peerconnection.identity.timeout'] = 1;
                extraPrefsFirefox['media.peerconnection.turn.disable'] = true;
                extraPrefsFirefox['media.peerconnection.use_document_iceservers'] = false;
                extraPrefsFirefox['media.peerconnection.video.enabled'] = false;
                extraPrefsFirefox['privacy.donottrackheader.enabled'] = true;
                extraPrefsFirefox['privacy.trackingprotection.enabled'] = true;
            }
            if (req.query.user_agent) {
                console.log('User-Agent: ' + req.query.user_agent + ' (' + req.params.url + ')');
                extraPrefsFirefox['general.useragent.override'] = req.query.user_agent;
            }
            launch_params = {
                headless: true,
                args: firefox_args,
                ignoreHTTPSErrors: true,
                product: 'firefox',
                extraPrefsFirefox: extraPrefsFirefox
            };
        }

        const browser = await puppeteer.launch(launch_params).catch(err => {
            console.log(err.toString());
            errors.push(err.toString());
        });

        const page = await browser.newPage().catch(err => {
            errors.push(err.toString());
        });

        // Disable javascript resources
        if (req.query.jsoff) {
            console.log('Disable JS script resources');
            await page.setRequestInterception(true).catch(err => {
                errors.push(err.toString());
            });
            page.on('request', request => {
                if (request.resourceType() === 'script') {
                    request.abort();
                } else {
                    request.continue();
                }
            });
        }

        // Filter requests
        if (req.query.refilter) {
            console.log('Filtering requests that match the expression: ' + req.query.refilter);
            await page.setRequestInterception(true).catch(err => {
                errors.push(err.toString());
            });
            page.on('request', request => {
                let reqURL = request.url();
                if (reqURL == req.params.url) {
                    request.continue();
                    return
                }
                if (request.resourceType() === 'script') {
                    // You can disable the scripts using "jsoff" option
                    // we want this enable to AJAX working by default
                    request.continue()
                    return
                }
                if (!reqURL.match(req.query.refilter)) {
                    request.abort();
                } else {
                    request.continue();
                }
            });
        }

        // AJAX call URL regex
        let ajaxres = [];
        let ajaxurl = [];
        if (req.query.reajax) {
            console.log('Detecting AJAX calls that match the expression: ' + req.query.reajax);
            page.on('response', async response => {
                let resURL = response.url();
                if (resURL.match(req.query.reajax)) {
                    console.log('AJAX call found')
                    let body = await response.text().catch(err => {
                        errors.push(err.toString());
                    });
                    if (body) {
                        ajaxurl.push(resURL);
                        ajaxres.push(body);
                    }
                }
            });
        }

        // USER AGENT for Chromium
        if (req.query.user_agent && !req.query.firefox) {
            console.log('User-Agent: ' + req.query.user_agent + ' (' + req.params.url + ')');
            await page.setUserAgent(req.query.user_agent).catch(err => {
                errors.push(err.toString());
            });
        }

        // HEADERS
        if (req.query.headers) {
            console.log('Headers: ' + req.query.headers + '(' + req.params.url + ')');
            await page.setExtraHTTPHeaders(JSON.parse(req.query.headers)).catch(err => {
                errors.push(err.toString());
            });
        }

        // TIMEOUT
        if (req.query.timeout) {
            console.log('Setting timeout to ' + req.query.timeout + ' miliseconds');
            page.setDefaultTimeout(Number(req.query.timeout));
            page.setDefaultNavigationTimeout(Number(req.query.timeout));
        }

        // Cookies
        if (req.method === 'POST') {
            if (req.body.cookies) {
                const cookies = JSON.parse(req.body.cookies);
                console.log('Setting cookies');
                await page.setCookie(...cookies).catch(err => {
                    errors.push(err.toString());
                });
            }
        }

        let response = undefined;

        let waitUntil = 'domcontentloaded';
        if (req.query.waituntil) {
            waitUntil = req.query.waituntil;
        }

        // GO TO PAGE
        if (req.query.fullpage) {
            console.log('Full page: ' + req.params.url);
            response = await page.goto(req.params.url).catch(err => {
                errors.push(err.toString());
            });
        } else {
            console.log('Wait until: ' + waitUntil);
            response = await page.goto(req.params.url, {
                waitUntil: waitUntil.split(',')
            }).catch(err => {
                errors.push(err.toString());
            });
        }

        // wait for timeout
        if (req.query.wait) {
            const waitT = Number(req.query.wait);
            console.log('Waiting ' + waitT + ' miliseconds');
            await page.waitForTimeout(waitT).catch(err => {
                errors.push(err.toString());
            });
        }

        return [browser, page, response, ajaxres, ajaxurl, errors];
    },
    set_error_response: function (req, res, errors) {
        res.setHeader('Content-Type', 'application/json');
        res.status(500);
        res.send({
            message: 'All Done!',
            errors: errors,
            data: {
                url: req.params.url,
                body: '',
                cookies: [],
                evaluate: null,
                screenshot: null
            }});
    }
}
