const bodyParser = require('body-parser');

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

const brw = require('../lib/browser');


function delay(t, val) {
    return new Promise(function(resolve) {
        setTimeout(function() {
            resolve(val);
        }, t);
    });
}


async function process_main_request(req, res, puppeteer) {
    const main_errors = [];  // errors in the main browser view

    const [browser, page, response, ajaxres, ajaxurl, errors] = await brw.get_page_main_view(req, puppeteer).catch(err => {
        main_errors.push(err.toString());
    });

    if (main_errors.length) {
        brw.set_error_response(req, res, main_errors);
        return;
    }

    // CLICK
    if (req.query.click) {
        console.log('Click: "' + req.query.click + '"');
        await page.click(req.query.click).then(async _ => {
            let wait = 5000;
            if (req.query.click_wait) {
                wait = Number(req.query.click_wait);
            }
            console.log('Waiting ' + wait + ' miliseconds');
            await page.waitForTimeout(wait).catch(err => {
                errors.push(err.toString());
            });
        }).catch(err => {
            errors.push(err.toString());
        });
    }

    // FORM
    if (req.query.form) {
        console.log('Trying to submit form: ' + req.query.form);
        let form;
        try {
            form = JSON.parse(req.query.form);
        } catch (err) {
            brw.set_error_response(req, res, [err.toString()]);
            return;
        }
        if (form.fields) {
            for (let fsel in form.fields) {
                await page.type(fsel, form.fields[fsel]).catch(err => {
                    errors.push(err.toString());
                });
            }
        }
        if (form.select) {
            if (form.select.__order__) {
                for (let six in form.select.__order__) {
                    let ssel = form.select.__order__[six]
                    console.log('FORM: select "' + ssel + '" as value "' + form.select[ssel] + '"')
                    await page.select(ssel, form.select[ssel]).then(async _ => {
                        await delay(5000);
                    }).catch(err => {
                        errors.push(err.toString());
                    });
                }
            } else {
                for (let ssel in form.select) {
                    console.log('FORM: select "' + ssel + '" as value "' + form.select[ssel] + '"')
                    await page.select(ssel, form.select[ssel]).then(async _ => {
                        await delay(2000);
                    }).catch(err => {
                        errors.push(err.toString());
                    });
                }
            }
        }
        await page.click(form.submit).catch(err => {
            errors.push(err.toString());
        });
        await page.waitForNavigation().catch(err => {
            errors.push(err.toString());
        });
        console.log('Done!');
    }

    // EVALUATE
    let evaluateResult = {};
    if (req.query.evaluate) {
        console.log('Evaluate: ' + req.query.evaluate);
        evaluateResult = await page.evaluate((expresion) => eval(expresion), req.query.evaluate).catch(err => {
            errors.push(err.toString());
        });
    }

    // Screenshot base64
    let screenshot = '';
    if (req.query.screenshot) {
        console.log('Taking screenshot as base64');
        screenshot = await page.screenshot({encoding: 'base64', fullPage: true }).catch(err => {
            errors.push(err.toString());
        });
    }

    const body = await page.content().catch(err => {
        errors.push(err.toString());
    });
    const cookies = await page.cookies().catch(err => {
        errors.push(err.toString());
    });

    await browser.close().catch(err => {
        errors.push(err.toString());
    });

    res.setHeader('Content-Type', 'application/json');
    if (response) {
        res.status(response.status());
    } else {
        res.status(500);
    }

    if (errors.some((err) => err.includes("TimeoutError"))) {
        res.status(504);
    }

    res.send({
        message: 'All Done!',
        errors: errors,
        data: {
            url: page.url(),
            body: body,
            cookies: cookies,
            evaluate: evaluateResult,
            ajaxres: ajaxres,
            ajaxurl: ajaxurl,
            screenshot: screenshot
        }});
}


module.exports = function(app, puppeteer) {
    app.get('/:url', async function(req, res) {
        await process_main_request(req, res, puppeteer);
    });
    app.post('/:url', urlencodedParser, async function(req, res) {
        await process_main_request(req, res, puppeteer);
    });
}
