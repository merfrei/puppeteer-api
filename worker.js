const express = require('express');

const app = express();

const puppeteer = require('puppeteer-extra');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());


require('./config.js')(app, express);
require('./views/main.js')(app, puppeteer);

const args = process.argv;

let port = 3000;

if (args.length > 2) {
    port = args[2];
}

console.log('Listening on port ' + port);

app.listen(Number(port));
